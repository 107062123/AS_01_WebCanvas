<style>
table th{
    width: 100%;
}
</style>
# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| color picker                                  | 1~5%     | Y         |
| fill                                  | 1~5%     | Y         |


---

### How to use 

 點擊工具欄的圖示即可切換工具，點擊後工具欄會將圖示背景顏色切換以告知使用者目前正在使用哪個工具。
* ![](https://i.imgur.com/PVQqKzN.png)\
唯一不同的是此工具點擊後會維持亮著的狀態直到再次點擊以取消此功能。此工具的功能為填滿矩形、圓形、三角形，以及使用畫筆時會將原點和終點連接在一起並將包圍起來的地方填滿，使用橡皮擦時也有同樣效果，效果變為擦掉起點和終點包含的區塊。\
* ![](https://i.imgur.com/hHe0L9y.jpg)\
將滑鼠游標移至上圖其中一個區塊即可改變字體及文字大小。\
* ![](https://i.imgur.com/QuTSE6f.png)\
拉動上圖旁邊的滑條即可改變畫筆、橡皮擦以及各圖形邊框的大小。\
* ![](https://i.imgur.com/AEtC3fh.png)\
點選上圖工具後再點選圖內的的區塊即可獲得該區塊的顏色。
* ![](https://i.imgur.com/851OSmY.jpg) \
點選區塊內任一位置即可更改顏色，並且會將選擇的顏色紀錄在下方的
![](https://i.imgur.com/ZuI0tWT.jpg)







### Function description

    function colorPick(evt){
        if(!tool[6])return;
        var x = evt.offsetX;
        var y = evt.offsetY;
        var imgData = ctx.getImageData(x,y,1,1).data;
        var r = imgData[0], g = imgData[1], b = imgData[2];
        rgbaColor = 'rgba(' + r + "," + g + "," + b + ",1)";
        ctx.strokeStyle = rgbaColor;
        ctx.fillStyle = rgbaColor;
        nowColor();
        fillBlock();
        console.log(rgbaColor);
    }
 此function的功能為，若選擇了顏色選擇器這個工具，在點擊畫布時會取的點擊位置的顏色。

     var filler = getElement('fill');
     getElement('fill').addEventListener('click', function () {
        fill = !fill;
        if(fill){
            toolHovered.bind(filler)();
        }else{
            notHovered.bind(filler)();
        }
    });
此function的功能為填滿，當fill為true時，在畫圖形時會變為實心的，使用畫筆及橡皮擦時會將起點與終點的位置連接在一起並將內容填滿。

### Gitlab page link

https://107062123.gitlab.io/AS_01_WebCanvas
