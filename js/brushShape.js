var startImage;
var startX = 0, startY = 0;
var lastX = 0, lastY = 0;
var isDown = false;
var fill = false;

/*---------------------Draw a rectangle---------------------------------*/
canvas.addEventListener('mousedown', startRect);
canvas.addEventListener('mouseup', stopRect);
function startRect(evt) {
    startX = evt.offsetX;
    startY = evt.offsetY;
    if (tool[3]) {
        isDown = true;
        startImage = ctx.getImageData(0, 0, canvas.width, canvas.height);
        lastImage.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
        canvas.addEventListener('mousemove', drawRect);
        canvas.addEventListener('mouseout', stopRect);
    }
}
function stopRect(evt) {
    if (isDown) {
        nextImage.splice(0, nextImage.length);
    }
    isDown = false;
    canvas.removeEventListener('mousemove', drawRect);
    canvas.removeEventListener('mouseout', stopRect);
}
function drawRect(evt) {
    var x = evt.offsetX;
    var y = evt.offsetY;
    var w = lastX - startX;
    var h = lastY - startY;
    if (isDown) {
        ctx.putImageData(startImage, 0, 0);
        w = x - startX;
        h = y - startY;
        if (fill) ctx.fillRect(startX, startY, w, h);
        else ctx.strokeRect(startX, startY, w, h);
        lastX = x;
        lastY = y;
    }
}
/*-------------------------------------------------------------------------*/

/*-------------------Draw a triangle---------------------------------------*/
canvas.addEventListener('mousedown', startTri);
canvas.addEventListener('mouseup', stopTri);
function startTri(evt) {
    startX = evt.offsetX;
    startY = evt.offsetY;
    if (tool[4]) {
        isDown = true;
        startImage = ctx.getImageData(0, 0, canvas.width, canvas.height);
        lastImage.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
        canvas.addEventListener('mousemove', drawTri);
        canvas.addEventListener('mouseout', stopTri);
    }
}
function stopTri(evt) {
    if (isDown) {
        nextImage.splice(0, nextImage.length);
    }
    isDown = false;
    canvas.removeEventListener('mousemove', drawTri);
    canvas.removeEventListener('mouseout', stopTri);
}
function drawTri(evt) {
    var x = evt.offsetX;
    var y = evt.offsetY;
    var topVertex = {
        x: (x + startX) / 2,
        y: (y > startY) ? startY : y
    };
    var leftVertex = {
        x: (x > startX) ? startX : x,
        y: (y > startY) ? y : startY
    };
    var rightVertex = {
        x: (x > startX) ? x : startX,
        y: (y > startY) ? y : startY
    }
    if (isDown) {
        ctx.putImageData(startImage, 0, 0);
        ctx.beginPath();
        ctx.moveTo(topVertex.x, topVertex.y);
        ctx.lineTo(leftVertex.x, leftVertex.y);
        ctx.lineTo(rightVertex.x, rightVertex.y);
        ctx.closePath();
        if (fill) ctx.fill();
        else ctx.stroke();
    }
}
/*-------------------------------------------------------------------------*/

/*------------------------Draw a circle------------------------------------*/
canvas.addEventListener('mousedown', startCircle);
canvas.addEventListener('mouseup', stopCircle);
function startCircle(evt) {
    startX = evt.offsetX;
    startY = evt.offsetY;
    if (tool[5]) {
        isDown = true;
        startImage = ctx.getImageData(0, 0, canvas.width, canvas.height);
        lastImage.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
        canvas.addEventListener('mousemove',drawCircle);
        canvas.addEventListener('mouseout', stopCircle);
    }
}
function stopCircle(evt) {
    if (isDown) {
        nextImage.splice(0, nextImage.length);
    }
    isDown = false;
    canvas.removeEventListener('mousemove', drawCircle);
    canvas.removeEventListener('mouseout', stopCircle);
}
function drawCircle(evt) {
    var x = evt.offsetX;
    var y = evt.offsetY;
    x = Math.pow(x - startX, 2);
    y = Math.pow(y - startY, 2);
    var radius = Math.sqrt(x + y) / 2;
    if (isDown) {
        x = (evt.offsetX + startX) / 2;
        y = (evt.offsetY + startY) / 2;
        ctx.putImageData(startImage, 0, 0);
        ctx.beginPath();
        ctx.arc(x, y, radius, 0, 2 * Math.PI);
        if (fill) ctx.fill();
        else ctx.stroke();
    }
}
/*-------------------------------------------------------------------------*/
var filler = getElement('fill');
getElement('fill').addEventListener('click', function () {
    fill = !fill;
    if(fill){
        toolHovered.bind(filler)();
    }else{
        notHovered.bind(filler)();
    }
});