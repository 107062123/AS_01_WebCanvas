var colorBlock = document.getElementById('color-block');
var ctx1 = colorBlock.getContext('2d');
var width1 = colorBlock.width;
var height1 = colorBlock.height;

var colorStrip = document.getElementById('color-strip');
var ctx2 = colorStrip.getContext('2d');
var width2 = colorStrip.width;
var height2 = colorStrip.height;

var rgbaColor = "rgba(0,0,0,1)"
fillBlock();
nowColor();

//fill strip
var grdOfStrip = ctx2.createLinearGradient(0,0,0,height2);
grdOfStrip.addColorStop(0,'rgba(255,0,0,1)');
grdOfStrip.addColorStop(0.17,'rgba(255,255,0,1)');
grdOfStrip.addColorStop(0.34,'rgba(0,255,0,1)');
grdOfStrip.addColorStop(0.51,'rgba(0,255,255,1)');
grdOfStrip.addColorStop(0.68,'rgba(0,0,255,1)');
grdOfStrip.addColorStop(0.85,'rgba(255,0,255,1)');
grdOfStrip.addColorStop(1,'rgba(255,0,0,1)');
ctx2.fillStyle = grdOfStrip;
ctx2.fillRect(0,0,width2,height2);

function fillBlock(){
    ctx1.fillStyle = rgbaColor;
    ctx1.fillRect(0,0,width1,height1);

    var grdWhite = ctx1.createLinearGradient(0,0,width1,0);
    grdWhite.addColorStop(0,'rgba(255,255,255,1)');
    grdWhite.addColorStop(1,'rgba(255,255,255,0)');
    ctx1.fillStyle = grdWhite;
    ctx1.fillRect(0,0,width1,height1);

    var grdBlack = ctx1.createLinearGradient(0,0,0,height1);
    grdBlack.addColorStop(0,"rgba(0,0,0,0)");
    grdBlack.addColorStop(1,"rgba(0,0,0,1)");
    ctx1.fillStyle = grdBlack;
    ctx1.fillRect(0,0,width1,height1);
}

function clickStrip(evt){
    var x = evt.offsetX;
    var y = evt.offsetY;
    var imgData = ctx2.getImageData(x,y,1,1).data;
    var r = imgData[0], g = imgData[1], b = imgData[2];
    rgbaColor = 'rgba(' + r + "," + g + "," + b + ",1)";
    ctx.strokeStyle = rgbaColor;
    ctx.fillStyle = rgbaColor;
    fillBlock();
    nowColor();
}

colorStrip.addEventListener('click',function(evt){
    clickStrip(evt);
});

function clickBlock(evt){
    var x = evt.offsetX;
    var y = evt.offsetY;
    var imgData = ctx1.getImageData(x,y,1,1).data;
    var r = imgData[0], g = imgData[1], b = imgData[2];
    rgbaColor = 'rgba(' + r + "," + g + "," + b + ",1)";
    ctx.strokeStyle = rgbaColor;
    ctx.fillStyle = rgbaColor;
    nowColor();
}
colorBlock.addEventListener('click',function(evt){
    clickBlock(evt);
});
/**********************************************************************************/

/*-------------------------color piacker------------------------------------------*/
canvas.addEventListener('click',colorPick);
function colorPick(evt){
    if(!tool[6])return;
    var x = evt.offsetX;
    var y = evt.offsetY;
    var imgData = ctx.getImageData(x,y,1,1).data;
    var r = imgData[0], g = imgData[1], b = imgData[2];
    rgbaColor = 'rgba(' + r + "," + g + "," + b + ",1)";
    ctx.strokeStyle = rgbaColor;
    ctx.fillStyle = rgbaColor;
    nowColor();
    fillBlock();
    console.log(rgbaColor);
}

function nowColor(){
    var x = getElement('now-color');
    var y = x.getContext('2d');
    y.fillStyle = rgbaColor;
    y.fillRect(0,0,x.width,x.height);
}
