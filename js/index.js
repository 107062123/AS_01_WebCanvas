var canvas = document.getElementById('art');//canvas element
var ctx = canvas.getContext('2d');//context of canvas
var sizeOfTool = document.getElementById('sizeOfTool');//the range of brush/eraser element

/*-------------init-----------------------------*/
canvas.style.cursor = "url(img/pencil_burned.png),auto";//defaut cursor (brush)
ctx.fillStyle = "white";
ctx.fillRect(0,0,canvas.width,canvas.height);
ctx.fillStyle = "black";

//get mouse's position
function getMousePos(evt) {
    return {
        x: evt.offsetX,
        y: evt.offsetY
    };
}

function getElement(str){
    return document.getElementById(str);
}

/*-----------------------------Draw and erase------------------------------------------*/
canvas.addEventListener('mousedown', startDrawErase);
canvas.addEventListener('mouseup', stopDrawErase);
function drawErase(evt) {
    var mousePos = getMousePos(evt);
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.stroke();
}
function stopDrawErase(evt){
    if(tool[0] || tool[1]){
        if(fill){
            ctx.closePath();
            ctx.fill();
        }
        canvas.removeEventListener('mouseout',stopDrawErase);
        canvas.removeEventListener('mousemove', drawErase, false);
        nextImage.splice(0,nextImage.length);
    }
}
function startDrawErase(evt){
    var mousePos = getMousePos(evt);
    ctx.lineWidth = sizeOfTool.value;
    if (tool[0] || tool[1]) {//brush
        var img = ctx.getImageData(0,0,canvas.width,canvas.height);
        ctx.fillRect(mousePos.x,mousePos.y,3,3);
        lastImage.push(img);
        ctx.beginPath();
        ctx.moveTo(mousePos.x, mousePos.y);
        canvas.addEventListener('mousemove', drawErase, false);
        canvas.addEventListener('mouseout',stopDrawErase);
    }
}
/*------------------------------------------------------------------------------------*/

/*----------------- undo and redo ----------------------------------------------------*/
var lastImage = [];
var nextImage = [];
function undo(){
    var img = lastImage.pop();
    if(img){
        var img2 = ctx.getImageData(0,0,canvas.width,canvas.height);
        nextImage.push(img2);
        ctx.putImageData(img,0,0);
    }
}
function redo(){
    var img = nextImage.pop();
    console.log(img);
    if(img){
        var img2 = ctx.getImageData(0,0,canvas.width,canvas.height);
        lastImage.push(img2);
        ctx.putImageData(img,0,0);
    }
}
document.getElementById('undo').addEventListener('click',undo);
document.getElementById('redo').addEventListener('click',redo);
/*--------------------------------------------------------------------------*/

//reset the canvas
function clearAll() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    lastImage.splice(0,lastImage.length);
    nextImage.splice(0,nextImage.length);
    var temp = ctx.fillStyle;
    ctx.fillStyle = "white";
    ctx.fillRect(0,0,canvas.width,canvas.height);
    ctx.fillStyle = temp;
}

