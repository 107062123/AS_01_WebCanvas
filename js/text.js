var fontFace = "Arial";
var fontSize = "16px ";

/*------------------------initial font face's function and listner-------------------*/
function closeFontFace(){
    getElement('fontFaceCtx').style.display = 'none';
}

function openFontFace(){
    getElement('fontFaceCtx').style.display = 'block';
}

function changeFontFace(){
    fontFace = this.innerHTML;
    getElement('fontFaceBtn').innerHTML = fontFace;
    closeFontFace();
}

function initFontFace(){
    getElement('fontFace').addEventListener('mousemove',openFontFace);
    getElement('fontFace').addEventListener('mouseout', closeFontFace);
    getElement('Arial').addEventListener('click',changeFontFace);
    getElement('Verdana').addEventListener('click',changeFontFace);
    getElement('Courier New').addEventListener('click',changeFontFace);
}
initFontFace();
/*---------------------------------------------------------------------------------*/

/*---------------------initial font size's function and add listener------------------*/
function closeFontSize(){
    getElement('fontSizeCtx').style.display = 'none';
}

function openFontSize(){
    getElement('fontSizeCtx').style.display = 'block';
}

function changeFontSize(){
    fontSize = this.innerHTML + "px ";
    getElement('fontSizeBtn').innerHTML = this.innerHTML;
    closeFontSize();
}

function initFontSize(){
    getElement('fontSize').addEventListener('mousemove',openFontSize);
    getElement('fontSize').addEventListener('mouseout', closeFontSize);
    getElement('size12').addEventListener('click',changeFontSize);
    getElement('size14').addEventListener('click',changeFontSize);
    getElement('size16').addEventListener('click',changeFontSize);
    getElement('size18').addEventListener('click',changeFontSize);
    getElement('size20').addEventListener('click',changeFontSize);
    getElement('size22').addEventListener('click',changeFontSize);
    getElement('size24').addEventListener('click',changeFontSize);
    getElement('size26').addEventListener('click',changeFontSize);
    getElement('size28').addEventListener('click',changeFontSize);
    getElement('size30').addEventListener('click',changeFontSize);
    getElement('size32').addEventListener('click',changeFontSize);
    getElement('size34').addEventListener('click',changeFontSize);
    getElement('size36').addEventListener('click',changeFontSize);
    getElement('size38').addEventListener('click',changeFontSize);
    getElement('size40').addEventListener('click',changeFontSize);
}
initFontSize();
/*---------------------------------------------------------------------------------*/

function drawText(txt,mousePos,evt){
    if(evt.keyCode == 13){
        nextImage.splice(0,nextImage.length);
        var img = ctx.getImageData(0,0,canvas.width,canvas.height);
        lastImage.push(img);
        ctx.font = fontSize + fontFace;
        var x = Number(mousePos.x.substr(0,mousePos.x.length-2));
        var y = Number(mousePos.y.substr(0,mousePos.y.length-2));
        ctx.fillText(txt.value, x, y);
        txt.remove();
    }
}

function typeText(txt,evt){
  var mousePos = getMousePos(evt);
  mousePos.x = String(mousePos.x) + "px";
  mousePos.y = String(mousePos.y) + "px";
  txt.style.position = 'absolute';
  txt.style.left = mousePos.x;
  txt.style.top = mousePos.y;
  document.getElementById('demo').appendChild(txt);
  txt.focus();
  txt.addEventListener('keydown',function(evt){
      drawText(this, mousePos, evt);
  })
  canvas.addEventListener('click',function(){
        txt.remove();
  })
  window.addEventListener('mouseup',function(){
      txt.remove();
  });
}

canvas.addEventListener('click',function(evt){
    if(tool[2]){
        var txt = document.createElement('input');
        typeText(txt,evt);
    }
});