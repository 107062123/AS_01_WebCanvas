var tool = [true, false, false, false, false, false, false];
var lastTool = 0
//[brush, eraser, text, rect, tri, circle, colorPicker]
var brush = getElement('brush');
var eraser = getElement('eraser');
var text = getElement('text');
var rect = getElement('rectangle');
var tri = getElement('triangle');
var circle = getElement('circle');
var colorPicker = getElement('colorPicker');
var toolList = [brush, eraser, text, rect, tri, circle, colorPicker];

toolHovered.bind(brush)();

/*-----------------------change tool-------------------------------------------------------------*/
function changeTool(now, last) {
    if(now != last){
        toolList[last].addEventListener('mouseout',notHovered);
        toolList[last].style.backgroundColor = "#cccccc";
        toolList[last].style.width = "64px";
        toolList[last].style.height = "64px";
        toolList[last].style.border = "none";
    }
    toolList[now].removeEventListener('mouseout', notHovered);
    for (i = 0; i < tool.length; i++) {
        if (i == now) tool[i] = true;
        else tool[i] = false;
    }
}

function toolHovered(){
    this.style.backgroundColor = "#ffffff";
    this.style.width = "60px";
    this.style.height = "60px";
    this.style.border = "2px inset";
}

function notHovered(){
    this.style.backgroundColor = "#cccccc";
    this.style.width = "64px";
    this.style.height = "64px";
    this.style.border = "none";
}
/*----------------------mouse click-----------------------------*/
brush.addEventListener('click', function () {
    toolHovered.bind(brush)();
    changeTool(0, lastTool);
    canvas.style.cursor = "url(img/pencil_burned.png),auto";
    ctx.globalCompositeOperation = "source-over";
    lastTool = 0;
});
document.getElementById('eraser').addEventListener('click', function () {
    toolHovered.bind(eraser)();
    changeTool(1,lastTool);
    canvas.style.cursor = "url(img/eraser_burned.png),auto";
    ctx.globalCompositeOperation = "destination-out";
    lastTool = 1;
});
document.getElementById('text').addEventListener('click', function () {
    toolHovered.bind(text)();
    changeTool(2,lastTool);
    ctx.globalCompositeOperation = "source-over";
    canvas.style.cursor = "text";
    lastTool = 2;
});
document.getElementById('rectangle').addEventListener('click', function () {
    toolHovered.bind(rect)();
    changeTool(3, lastTool);
    canvas.style.cursor = "url(img/rectangle_cursor.png),auto";
    ctx.globalCompositeOperation = "source-over";
    lastTool = 3;
});
document.getElementById('triangle').addEventListener('click',function(){
    toolHovered.bind(tri)();
    changeTool(4,lastTool);
    canvas.style.cursor = "url(img/triangle_cursor.png), auto";
    ctx.globalCompositeOperation = "source-over";
    lastTool = 4;
});
document.getElementById('circle').addEventListener('click',function(){
    toolHovered.bind(circle)();
    changeTool(5,lastTool);
    canvas.style.cursor = "url(img/circle_cursor.png), auto";
    ctx.globalCompositeOperation = "source-over";
    lastTool = 5;
});
getElement('colorPicker').addEventListener('click',function(){
    toolHovered.bind(colorPicker)();
    changeTool(6,lastTool);
    canvas.style.cursor = "url(img/picker_cursor.png), auto";
    lastTool = 6;
});

/*--------------------------------------------------------------------------------*/
brush.addEventListener('mousemove',toolHovered.bind(brush));
eraser.addEventListener('mousemove',toolHovered.bind(eraser));
text.addEventListener('mousemove',toolHovered);
rect.addEventListener('mousemove',toolHovered);
tri.addEventListener('mousemove', toolHovered);
circle.addEventListener('mousemove',toolHovered);
colorPicker.addEventListener('mousemove',toolHovered);

eraser.addEventListener('mouseout', notHovered);
text.addEventListener('mouseout',notHovered);
rect.addEventListener('mouseout',notHovered);
tri.addEventListener('mouseout', notHovered);
circle.addEventListener('mouseout',notHovered);
colorPicker.addEventListener('mouseout',notHovered);